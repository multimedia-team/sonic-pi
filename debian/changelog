sonic-pi (3.2.2~repack-11) unstable; urgency=medium

  * Replace deprecated import of mocha/setup (Closes: #1090882, #1093941)

 -- Valentin Vidic <vvidic@debian.org>  Fri, 24 Jan 2025 22:36:24 +0100

sonic-pi (3.2.2~repack-10) unstable; urgency=medium

  * Add Depends on jack-example-tools (Closes: #1079808)

 -- Valentin Vidic <vvidic@debian.org>  Mon, 16 Sep 2024 23:34:12 +0200

sonic-pi (3.2.2~repack-9) unstable; urgency=medium

  [ Valentin Vidic ]
  * Revert v4 changes from master branch
  * Update pkgconf build dependency
  * Move jackd to Recommends (Closes: #1023704)
  * Fix clean rules (Closes: #1049503)
  * Update Standards-Version to 4.7.0
  * Remove Georges Khaznadar from Uploaders
  * Update lintian-overrides tag name
  * Update list of files to clean (Closes: #1048239)

  [ Lucas Kanashiro ]
  * Fix FTBFS with Ruby 3.2

 -- Valentin Vidic <vvidic@debian.org>  Mon, 13 May 2024 23:10:42 +0200

sonic-pi (3.2.2~repack-8) unstable; urgency=medium

  [ Valentin Vidic ]
  * Ignore stderr from jackd in gui test
  * Set supported architectures for tests
  * Ignore stderr from jackd in server test (Closes: #982068)
  * Update Standards-Version
  * Update copyright year for debian files
  * Update linitian overrides

  [Georges Khaznadar]
  * bumped Standards-Version: 4.6.2

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 22 Feb 2023 14:22:49 +0100

sonic-pi (3.2.2~repack-7) unstable; urgency=medium

  * Try to ignore blhc warning for CMakeCXXCompilerABI.cpp
  * Update Standards-Version
  * Try to fix flaky gui test (Closes: #979620)

 -- Valentin Vidic <vvidic@debian.org>  Sat, 09 Jan 2021 21:47:54 +0100

sonic-pi (3.2.2~repack-6) unstable; urgency=medium

  * Add osmid and sox dependencies
  * Store ~/.sonic-pi into test artifacts
  * Update watch file version
  * Update lintian overrides

 -- Valentin Vidic <vvidic@debian.org>  Sun, 08 Nov 2020 16:26:48 +0100

sonic-pi (3.2.2~repack-5) unstable; urgency=medium

  * Try to make language list reproducible by sorting
  * Add Multi-Arch fields for arch-indep packages
  * Make build verbose for blhc check to work
  * Fix tutorial language name for debian-i18n

 -- Valentin Vidic <vvidic@debian.org>  Sat, 17 Oct 2020 11:40:59 +0200

sonic-pi (3.2.2~repack-4) unstable; urgency=medium

  * Update CC-BY-SA-4.0 license text (Closes: #903045)
  * Use clipboard for entering code in gui test
  * Drop patch for fixed aubio import
  * Try to make examples reproducible by sorting

 -- Valentin Vidic <vvidic@debian.org>  Tue, 29 Sep 2020 21:47:57 +0200

sonic-pi (3.2.2~repack-3) unstable; urgency=medium

  * Use debhelper 13
  * Try to make erlang build reproducible
  * Update lintian override tag
  * Set Rules-Requires-Root to no
  * Update gui test to record audio output

 -- Valentin Vidic <vvidic@debian.org>  Sun, 27 Sep 2020 14:02:10 +0200

sonic-pi (3.2.2~repack-2) unstable; urgency=medium

  * Enable Salsa CI
  * Fix ruby dependencies
  * Add autopkgtest for server and gui
  * Fix deprecated file existence check

 -- Valentin Vidic <vvidic@debian.org>  Sun, 20 Sep 2020 23:33:33 +0200

sonic-pi (3.2.2~repack-1) unstable; urgency=medium

  * New upstream version 3.2.2~repack (Closes: #931144)
  * Prepare Files-Excluded for import of new version
  * Update patches for new version
  * Update dependencies for new version
  * Update copyright paths for new version
  * Update build rules for new version
  * Update installation paths for new version
  * Update Lintian overrides for new version
  * Add myself to Uploaders

 -- Valentin Vidic <vvidic@debian.org>  Sun, 20 Sep 2020 12:28:19 +0200

sonic-pi (2.10.0~repack-3) unstable; urgency=medium

  * changed Architecture: any to
    Architecture: i386 amd64 arm64 armhf mipsel to comply with architectures
    supported by the build-dependency supercollider-dev
  * udated Standards-Version: 4.5.0 , and dehelper-compat =12
  * replaced the dependency on libqt5scintilla2-dev => libqscintilla2-qt5-dev
    and the clause -lqt5scintilla2 => -lqscintilla2_qt5
  * fixed the deprecated calls to QString.sprinf. Closes: #964659

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 21 Aug 2020 19:05:19 +0200

sonic-pi (2.10.0~repack-2) unstable; urgency=medium

  [ Hanno Zulla ]
  * Add missing examples/ directory for Debian distribution package.

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 17 May 2016 18:16:41 +0000

sonic-pi (2.10.0~repack-1) unstable; urgency=medium

  [ Hanno Zulla ]
  * New upstream release 2.10 - 'Cowbell'.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 18 Apr 2016 09:37:11 +0000

sonic-pi (2.9.0~repack-6) unstable; urgency=medium

  * Build-depend on a version of ruby-wavefile working with big
    endian machines.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 27 Feb 2016 07:57:23 +0000

sonic-pi (2.9.0~repack-5) unstable; urgency=medium

  [ Hanno Zulla ]
  * don't load did_you_mean on Debian with ruby < 2.3.0

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 26 Feb 2016 16:50:24 +0100

sonic-pi (2.9.0~repack-4) unstable; urgency=medium

  [ Petter Reinholdtsen ]
  * Upgraded standards-version from 3.9.6 to 3.9.7.
  * Fixed typo in debian/copyright (Licence -> License).

  [ Hanno Zulla ]
  * Build-depend on sc3-plugins-server to avoid building on non-working
    architectures.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 26 Feb 2016 11:46:23 +0100

sonic-pi (2.9.0~repack-3) unstable; urgency=low

  * debian/copyright: Document zlib license for two hh files.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 19 Feb 2016 17:53:06 +0000

sonic-pi (2.9.0~repack-2) unstable; urgency=low

  * Remove duplicate lintian-overrides file.
  * Use ssl URIs for git repos.
  * Remove unused library from build-depends.

 -- Hanno Zulla <kontakt@hanno.de>  Fri, 19 Feb 2016 09:37:00 +0100

sonic-pi (2.9.0~repack-1) unstable; urgency=low

  * Initial packaging.
  * Packaging info in debian/README.source and debian/patches/*.patch.
  * Closes: #796550.

 -- Hanno Zulla <kontakt@hanno.de>  Fri, 12 Feb 2016 12:08:00 +0100
